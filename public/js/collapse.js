var collapse = (function() {

  var btn_collapse = document.getElementsByClassName('collapse-btn')
      ,i
      ,len = btn_collapse.length;

      for(i = 0; i < len; i++) {
        btn_collapse[i].addEventListener('click', function() {
          
          this.classList.toggle('active');
          var content = this.nextElementSibling;

          if(content.style.maxHeight) {
            content.style.maxHeight = null;
          } else {
            content.style.maxHeight = content.scrollHeight + "px";
          }
        });
      }
})();