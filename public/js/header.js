var scroll_page = (function() {
  
  var header = document.getElementById('header');
  var sticky = header.offsetTop;

  return window.onscroll = function() {

    if(window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > sticky) {
      header.classList.add('sticky');
    } else {
      header.classList.remove('sticky');
    }
  }
})();