const gulp = require('gulp')
      ,htmlmin = require('gulp-htmlmin')
      ,csso = require('gulp-csso')
      ,concat = require('gulp-concat')
      ,uglify = require('gulp-uglify')
      ,autoPrefixer = require('gulp-autoprefixer')
      ,del = require('del')
      ,runSequence = require('run-sequence')
      ,inlineSource = require('gulp-inline-source')
      ,removeCode = require('gulp-remove-code')
      ,vinylPaths = require('vinyl-paths');

const AUTOPREFIXER = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

gulp.task('html', () => {
    return gulp.src('./public/*.html')
        .pipe(removeCode({production: true}))
        .pipe(htmlmin({collapseWhitespace: true, removeComments: true}))
        .pipe(inlineSource())
        .pipe(gulp.dest('./build/'));
});

gulp.task('css', () => {
    return gulp.src('./public/css/*.css')
        .pipe(autoPrefixer({browsers: AUTOPREFIXER}))
        .pipe(csso())
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('js', () => {
    return gulp.src('./public/js/*.js')
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest('./public/js'));
});

gulp.task('copy', () => {
    return gulp.src('img/*', {cwd: 'public'})
        .pipe(gulp.dest('./build/img'));
});

gulp.task('deletefiles', () => {
    return gulp.src(['css/*.min.css', 'js/*.min.js'], {cwd: 'public'})
        .pipe(vinylPaths(del));
});

gulp.task('clean', () => del(['build']));

gulp.task('default', ['clean'], () =>
    runSequence('css', 'js', 'html', 'copy', 'deletefiles'));